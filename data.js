const mongoose = require("mongoose");

//création d'un schéma pour les variables
const schema  = mongoose.Schema({
    nom : String,
    mail : String,
    date_paiement : String,
    mode_paiement : String,
    montant : String
})

module.exports = mongoose.model('data', schema)