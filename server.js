const express = require("express");
const app = express();
const mongoose = require("mongoose");
const Data = require('./data'); // on importe notre model
const body = require('body-parser');

app.use(body());
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: false
};

//url base de données
const url = "mongodb://localhost:27017/paiement";
let port = 3000;

mongoose.connect(url).then(() => {
    console.log("Successfully connected to the database");  
    app.listen(port, () =>  { // ecoute du serveur sur le port 3000
        console.log('le serveur fonctionne');
    });  
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

app.post('/', async (req, res) => {
    const nom = req.body.nom; // récupération des variables du body
    const mail = req.body.mail
    const date_paiement = req.body.date_paiement
    const mode_paiement = req.body.mode_paiement
    const montant = req.body.montant
 
    if (!nom || !date_paiement  || !mail || !montant || !mode_paiement) { // on vérifie que les variables sont présentes
        res.send('Il manque un argument')
        return
    }
 
    const nouveau_paiement = new Data({ // création d'un objet représentant notre nouvelle entrée
        nom : nom,
        mail : mail,
        date_paiement : date_paiement,
        mode_paiement : mode_paiement,
        montant : montant
    })
     
    await nouveau_paiement.save() // sauvegarde asynchrone du nouveau paiement
    res.json(nouveau_paiement)
    return
});

app.get('/', async (req, res) => {
    const data = await Data.find() // On récupère tout les paiements
    await res.json(data)
})